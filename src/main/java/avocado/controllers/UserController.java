package avocado.controllers;

import avocado.models.User;
import avocado.models.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by johan on 2015-07-11.
 */
@Controller
public class UserController {
    @RequestMapping("/create")
    @ResponseBody
    public String create(String name, HttpServletRequest request) {
        User user = null;
        try {
            user = new User(name);
            userDao.save(user);
        }
        catch (Exception ex) {
            return "Error creating the user: " + ex.toString();
        }
        return "User succesfully created! (id = " + user.getId() + ")";
    }

    @Autowired
    private UserDao userDao;
}
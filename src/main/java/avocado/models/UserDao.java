package avocado.models;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by johan on 2015-07-11.
 */
@Transactional
public interface UserDao extends CrudRepository<User, Long> {
    public User findByName(String name);
}
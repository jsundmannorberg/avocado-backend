package avocado.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by johan on 2015-07-11.
 */
@Entity
@Table(name="users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    private String name;

    //@Temporal(TemporalType.TIMESTAMP)
    //@Column(name = "created_on", /*insertable = false,*/ columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    //private Date createdOn;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_login", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    private Date lastLoginTime;

    public User() {
    }

    public User(long id) {
        this.id = id;
    }

    public User(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //public Date getCreatedOn() {
    //    return createdOn;
   // }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }
}